/*  Table of Contents 
01. MENU ACTIVATION
02. MOBILE NAVIGATION ACTIVATION
03. FLEXSLIDER LANDING PAGE
04. SCROLL TO TOP BUTTON
05. Registration Page On/Off Clickable Items
*/
var ajaxRequest;

jQuery(document).ready(function($) {
	 'use strict';
	 var pathname=window.location.pathname;
	 var pathloc=window.location.search;
if(pathname===`/verification` && pathloc===`?do=mobile`){
	$('#LoginModal').modal({backdrop: 'static', keyboard: false})  

}
$( ".mmmbutton" ).click(function() {
  $( ".registration-social-login-container" ).toggle();
});
$("#phone").intlTelInput({
  initialCountry: "auto",
  geoIpLookup: function(callback) {
    $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
      var countryCode = (resp && resp.country) ? resp.country : "";
      callback(countryCode);
    });
  },
  utilsScript: "/asset/intl-tel-input-12.1.0/build/js/utils.js" // just for formatting/placeholders etc
});


$("#phone2").intlTelInput({
  initialCountry: "auto",
  geoIpLookup: function(callback) {
    $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
      var countryCode = (resp && resp.country) ? resp.country : "";
      callback(countryCode);
    });
  },
  utilsScript: "/asset/intl-tel-input-12.1.0/build/js/utils.js" // just for formatting/placeholders etc
});
var telInput = $("#phone"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");
telInput.intlTelInput({
  utilsScript: "/asset/intl-tel-input-12.1.0/build/js/utils.js"
});

var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hideMe");
  validMsg.addClass("hideMe");
};

// on blur: validate
telInput.bind("keyup change",function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hideMe");
            $("#sendotp").prop("disabled", false);

    } else {
      telInput.addClass("error");
      errorMsg.removeClass("hideMe");
            $("#sendotp").prop("disabled", true);

    }
  }
});

var telInput2 = $("#phone2"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");
telInput2.intlTelInput({
  utilsScript: "/asset/intl-tel-input-12.1.0/build/js/utils.js"
});

// on blur: validate
telInput2.bind("keyup change",function() {
  reset();
  if ($.trim(telInput2.val())) {
    if (telInput2.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hideMe");
            $("#sendotp").prop("disabled", false);

    } else {
      telInput2.addClass("error");
      errorMsg.removeClass("hideMe");
            $("#sendotp").prop("disabled", true);

    }
  }
});







// on keyup / change flag: reset

$('form').submit(function(event) { //Trigger on form submit
	errorMsg.addClass("hideMe");
	validMsg = $("#valid-msg");

$("#loader").removeClass('hideMe');
	 // Abort any pending ajaxRequest
    if (ajaxRequest) {
        ajaxRequest.abort();
    }
	var $form = $(this);

	//var postForm = $form.serialize();
	var action =$form.attr("action");
	var id =$form.attr("id");

	var method =$form.attr("method");
if(id==='LoginValidation'){
var intlNumber = $("#phone").intlTelInput("getNumber");
$("#phone").val(intlNumber);
$("#rephone").val(intlNumber);


}else if(id==='PinValidation'){
var intlNumber = $("#phone").intlTelInput("getNumber");
$("#rephone").val(intlNumber);

} else{
  var intlNumber = $("#phone2").intlTelInput("getNumber");
$("#phone2").val(intlNumber);
$("#rephone").val(intlNumber);

}



  
    // setup some local variables

    // Let's select and cache all the fields
    var $inputs = $form.find("input, select, button, textarea");

    // Serialize the data in the form
    var serializedData = $form.serialize();
    // Let's disable the inputs for the duration of the Ajax ajaxRequest.
    // Note: we disable elements AFTER the form data has been serialized.
    // Disabled form elements will not be serialized.
   // Prevent default posting of form - put here to work in case of errors
  $inputs.prop("disabled", true);
    // Fire off the ajaxRequest to /form.php
    ajaxRequest = $.ajax({
        url: action,
        type: method,
        data: serializedData,
		cache: false,
		processData:false

    });

    // Callback handler that will be called on success
    ajaxRequest.done(function (response, textStatus, jqXHR){
        /// Log a message to the console
 var res= JSON.parse(response);
console.log(res);

 if (res.status==='success' && (id==='LoginValidation' || id==='news' || id==='PinValidation') ) {
  $("#error-msg2").addClass('hideMe');
  $("#valid-msg").html(res.response);

  $("#valid-msg").removeClass('hideMe');

$("#loader").html(res.response);


$("#secondpromot").removeClass('hideMe');
$("#firstpromot").addClass('hideMe');
$inputs.prop("disabled", false);

}else if(res.status==='fail'){
  $("#error-msg2").html(res.response);
  $("#error-msg2").removeClass('hideMe');



}
else{
$inputs.prop("disabled", false);
$("#loader").addClass('hideMe');

		}
	



 });

    // Callback handler that will be called on failure
    ajaxRequest.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        var res= jqXHR;
				       $form.addClass("bounce animated");
					   $("#loader").addClass('hideMe');


     });
	 
    // Callback handler that will be called regardless
    // if the ajaxRequest failed or succeeded
    ajaxRequest.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
        $("#loader").addClass('hideMe');
  validMsg.addClass("hideMe");


    });
  // Prevent default posting of form
        event.preventDefault();
 });
/*
=============================================== 01. MENU ACTIVATION  ===============================================
*/
	 jQuery('nav#site-navigation-pro ul.sf-menu, nav#sidebar-nav ul.sf-menu').superfish({
			 	popUpSelector: 'ul.sub-menu, .sf-mega', 	// within menu context
	 			delay:      	200,                	// one second delay on mouseout
	 			speed:      	0,               		// faster \ speed
		 		speedOut:    	200,             		// speed of the closing animation
				animation: 		{opacity: 'show'},		// animation out
				animationOut: 	{opacity: 'hide'},		// adnimation in
		 		cssArrows:     	true,              		// set to false
			 	autoArrows:  	true,                    // disable generation of arrow mark-up
		 		disableHI:      true,
	 });
	 
	 
	 
	 /* Sticky Landing Page Header */
	 $('header.sticky-header').scrollToFixed({
		 minWidth:768
	 });
	 
	 
	 /* Remove Fixed Heading on Mobile */
 	$(window).resize(function() {
 	   var width_progression = $(document).width();
 	      if (width_progression < 768) {
			  $('header.sticky-header').trigger('detach.ScrollToFixed');
		  }
 	}).resize();
	 
	 /* Sitcky Video Sidebar */
	 $('nav#sidebar-nav.sticky-sidebar-js').hcSticky({
		 top:0
	 });
	 
	 
	 
/*
=============================================== 02. MOBILE NAVIGATION ACTIVATION  ===============================================
*/
 	$('#mobile-bars-icon-pro').click(function(e){
 		e.preventDefault();
 		$('#mobile-navigation-pro').slideToggle(350);
 		$("header#masthead-pro").toggleClass("active-mobile-icon-pro");
		$("header#videohead-pro").toggleClass("active-mobile-icon-pro");
 	});
	
	
   	$('ul#mobile-menu-pro').slimmenu({
   	    resizeWidth: '90000',
   	    collapserTitle: 'Menu',
   	    easingEffect:'easeInOutQuint',
   	    animSpeed:350,
   	    indentChildren: false,
   		childrenIndenter: '- '
   	});
	 
/*
=============================================== 03. FLEXSLIDER LANDING PAGE  ===============================================
*/
     $('.progression-studios-slider').flexslider({
 		slideshow: true,  		/* Autoplay True/False */
 		slideshowSpeed: 8000,	/* Autoplay Speed */
 		animation: "fade",		/* Slideshow Transition Animation */
 		animationSpeed: 800, 	/* Slide Transition Speed */
 		directionNav: true,		/* Left/Right Navigation True/False */
 		controlNav: true,		/* Bullet Navigaion True/False */
 		prevText: "",
 		nextText: "",
     });	 

	 	 
/*
=============================================== 04. SCROLL TO TOP BUTTON  ===============================================
*/
	 
   	// browser window scroll (in pixels) after which the "back to top" link is shown
   	var offset = 150,
  	
 	//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
   	offset_opacity = 1200,
  	
 	//duration of the top scrolling animation (in ms)
   	scroll_top_duration = 700,
  	
 	//grab the "back to top" link
   	$back_to_top = $('#pro-scroll-top');

 	//hide or show the "back to top" link
 	$(window).scroll(function(){
   		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
   		if( $(this).scrollTop() > offset_opacity ) { 
   			$back_to_top.addClass('cd-fade-out');
   		}
   	});

 	//smooth scroll to top
 	$back_to_top.on('click', function(event){
 		event.preventDefault();
 		$('body,html').animate({ scrollTop: 0 , }, scroll_top_duration
 	);
 	});


/*
=============================================== 05. Registration Page On/Off Clickable Items  ===============================================
*/
	
	$("ul.registration-invite-friends-list li").click(function() { 
	    $(this).closest("ul.registration-invite-friends-list li").toggleClass("active");
	});
	
	$("ul.registration-genres-choice li").click(function() { 
	    $(this).closest("ul.registration-genres-choice li").toggleClass("active");
	});



	

	 	 
});