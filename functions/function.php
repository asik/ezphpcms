<?php 
require_once ('config/templete.class.php');
require_once 'vendor/Facebook/autoload.php';

global $sid;

function replacecon($name){
$bad_str=$name;
$strong=preg_replace("/_con/i","",$bad_str);
$strong=preg_replace("/\/out/i","",$strong);
$strong=preg_replace("/\/out/i","",$strong);
return $strong;
}
function request_path()
{
    $request_uri = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
    $script_name = explode('/', trim($_SERVER['SCRIPT_NAME'], '/'));
    $parts = array_diff_assoc($request_uri, $script_name);
    if (empty($parts))
    {
        return '/';
    }
    $path = implode('/', $parts);
    if (($position = strpos($path, '?')) !== FALSE)
    {
        $path = substr($path, 0, $position);
    }
    return $path;
}

 /*FB API*/ 
 function CallAPI2($method, $api, $data) {
    $url = "https://graph.facebook.com/v2.11/$api/videos?fields=title,description,id,picture,length,created_time&limit=6&access_token=1495059160586826%7CGq2B81oIpHDOty07IXQlyqQDuac";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    switch ($method) {
        case "GET":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "POST":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE"); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            break;
    }
    $response = curl_exec($curl);
    $data = json_decode($response,true);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Check the HTTP Status code
    switch ($httpCode) {
        case 200:
            $error_status = "200: Success";
            return $data;
            break;
        case 404:
            $error_status = "404: API Not found";
            break;
        case 500:
            $error_status = "500: servers replied with an error.";
            break;
        case 502:
            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
            break;
        case 503:
            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
            break;
        default:
            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
            break;
    }
    curl_close($curl);
    echo $error_status;
    die;
}function robiaoc($method, $api, $data) {
    $url = "http://robi.mife-aoc.com/api/".$api;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    switch ($method) {
        case "GET":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "POST":
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE"); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            break;
    }
    $response = curl_exec($curl);
    $data = json_decode($response,true);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Check the HTTP Status code
    switch ($httpCode) {
        case 200:
            $error_status = "200: Success";
            return $data;
            break;
        case 404:
            $error_status = "404: API Not found";
            break;
        case 500:
            $error_status = "500: servers replied with an error.";
            break;
        case 502:
            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
            break;
        case 503:
            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
            break;
        default:
            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
            break;
    }
    curl_close($curl);
    echo $error_status;
    die;
}

 /*API REST POST FUNCTION*/
 function CallAPI($method, $api, $data) {
    $url = "https://api.leadne.com/index.php/" . $api;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			//print_r ($data);

    switch ($method) {
        case "GET":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "POST":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            break;
        case "DELETE":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE"); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            break;
    }
    $response = curl_exec($curl);
      //  print_r($response);

    $data = json_decode($response,true);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Check the HTTP Status code
    switch ($httpCode) {
        case 200:
            $error_status = "200: Success";
            return ($data);
            break;
        case 404:
            $error_status = "404: API Not found";
            break;
        case 500:
            $error_status = "500: servers replied with an error.";
            break;
        case 502:
            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
            break;
        case 503:
            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
            break;
        default:
            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
            break;
    }
    curl_close($curl);
    echo $error_status;
    die;
}
function generatePIN($digits = 4){
    $i = 0; //counter
    $pin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $pin .= mt_rand(0, 9);
        $i++;
    }
    return $pin;
}
function getids($param,$con){

$ids=CallAPI("GET",$param."?".$con,"");

$count=count($ids[$param]['records']);
if($count==1){
$splitted=$ids[$param]['records'][0][0];
$response=$splitted;
}elseif($count>1){
$str = implode (", ", $ids[$param]['records']);

$response=array($str);
}else {
    $response=false;
}


//echo implode(" ", $ids[$param][0]);

    
  //print_r($ids[$param]['records']);
    }

function delete($param,$ids,$con){
if($ids>0){
 $response=$ids;
    }else{
$ids=CallAPI("GET",$param."?".$con,"");
$count=count($ids[$param]['records']);
if($count==1){
$splitted=$ids[$param]['records'][0][0];
}elseif($count>1){
$splitted = explode(",", $ids[$param]['records']);
}else {$response=false;}

if($splitted){
  return delete($param,$splitted,'');
}


    }
    return $response;
    }
 function aouth($method,$link) {
    $url = $link;
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    $response = curl_exec($curl);
    //$data = json_decode($response,true);

    /* Check for 404 (file not found). */
    $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // Check the HTTP Status code
    switch ($httpCode) {
        case 200:
            $error_status = "200: Success";
            return ($response);
            break;
        case 404:
            $error_status = "404: API Not found";
            break;
        case 500:
            $error_status = "500: servers replied with an error.";
            break;
        case 502:
            $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
            break;
        case 503:
            $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
            break;
        default:
            $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
            break;
    }
    curl_close($curl);
    echo $error_status;
    die;
}
function get($param,$id){
return CallAPI("GET",$param."/".$id."?transform=1","");
}
function put($param,$id,$data){
return CallAPI("PUT",$param."/".$id,$data);
}
function getbyName($param,$name,$con,$val){
	$pai=$param."?filter=".$name.",".$con.",".$val."&transform=1";
    //print_r($pai);
return CallAPI("GET",$pai,"");
}
function auth($res){
		 
	    $userid=$res;
		$user=get('user_data',$userid);
		$sid="boom".md5($user['auth_token'].$user['uid'].$user['careated'].time());
		$dbs= array("sid"=>$sid,"expire"=>time()+84000);
        $data=put('user_data',$user['id'],$dbs);
		if($data){
		$_SESSION['sid']=$sid;
		$_SESSION['type']=$user['type'];
		$_SESSION['uid']=$user['uid'];
        $_SESSION['user']=$user['id'];
        $_SESSION['verify']=$user['mobileverification_id'];
        $_SESSION['auth']=$user['auth_token'];
        $_SESSION['state']=$user['state'];
		
		return true;
}else{
	
		return false;
}
}

function verify(){
	$sid=$_SESSION['sid'];
	$data=getbyName("user_data","sid","eq",$sid);
	if($data['user_data']=="" || $data['user_data'][0]=="" || $data['user_data'][0]['id']==""){
		return null;
	}elseif($data['user_data'][0]['uid']==$_SESSION['uid']){
		$dbs=["expire"=>time()+84000];
        $data=put('user_data',$_SESSION['user'],$dbs);
        if($data['user_data'][0]['mobileverification_id']==0){
                    $_SESSION['state']='verification?do=mobile';
					header('Location: /'.$_SESSION[state]);
					exit();
        }else{
                    $_SESSION['state']='exclusive';
                                header('Location: /'.$_SESSION[state]);
                                exit();
        }
		return true;
		}else{
			return false;
		}
	
}
function identify($msisd){
$re = '/([1-9]\d)+(01[5-9])/';
if( preg_match($re, $msisd, $matches))
{
	$data=array($matches);
}else{
	$respons="ABROAD";
}
	if($data[0][1]='88'){
		if($data[0][2]=='015'){
			$respons="TELETALK";
		}elseif($data[0][2]=='016'){
			$respons="AIRTEL";

		}elseif($data[0][2]=='017'){
					$respons="GP";

		}elseif($data[0][2]=='018'){
					$respons="ROBI";

		}elseif($data[0][2]=='019'){
							$respons="BANGLALINK";
		}else{
			$respons="other";
	}
	}else{
		$respons="ABROAD";
	}

		return $respons;


}
function logged(){
if (!isset($_SESSION['sid'])) {
	unset($_SESSION['sid']);
		unset( $_SESSION['type']);
		unset($_SESSION['uid']);
        unset($_SESSION['user']);
        unset($_SESSION['verify']);
        unset($_SESSION['auth']);
        unset($_SESSION['state']);
//header('Location: /home');
}else{
 $ses=($_SESSION);
 $data=verify();
 
// $data=getbyName("admin","sid","eq",$ses['sid']);
 
if($data){
	return true;
	}else{
		unset($_SESSION['sid']);
		unset( $_SESSION['type']);
		unset($_SESSION['uid']);
        unset($_SESSION['user']);
        unset($_SESSION['verify']);
        unset($_SESSION['auth']);
        unset($_SESSION['state']);

	return false;
	}
	
	


}
}
function login($email,$password)
{ 	 $table='admin';
	$method="GET";
	$query='filter[]=user,eq,'.$email.'&filter[]=pass,eq,'.$password.'&satisfy=all&transform=1';
	$api=$table.'?'.$query;
	//
	$result=null;
	$result= CallAPI($method, $api,'');
	
	if($result["admin"]==!''){
	//$result=json_decode(($result),true);
	//print_r( ($result));
	$result=($result["admin"][0]);
	$pass=($result['pass']);
	if($pass ==$password){
		$login=auth($result['id']);
		
		if($login){return $response_array=['status' => 'success','response' => 'You are successly logged in'] ;
		}else{
		return 	$response_array=['status' => 'danger','response' => 'Something went horrible'];}
		}
	else{
		return 	$response_array=['status' => 'warning','response' => 'User Password Error'];}
	}else{return $response_array=['status' => 'warning','response' => 'User and Password Combination Error'];}
	
}

function render($data,$path){
	$view = new Template();
	$view->title="Login";
	$view->data = $data;
	echo $view->render($path);
}function logout(){
	session_destroy(); 
//	header('Location: /home');

}function getSec($file){
	$time_seconds=0;
	    exec('ffmpeg -i'." upload/".$file." 2>&1 | grep Duration | awk '{print $2}' | tr -d ,",$O,$S);
if(!empty($O[0]))
    {
sscanf($O[0], "%d:%d:%d", $hours, $minutes, $seconds);
return $time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
	
	}else{
		return $time_seconds;
	}

}function getbit($sec){
	   $bitrate =(163840 / $sec);
if($bitrate>480){
	return $bitrate=480;
}else{
	return $bitrate;
}
}function ffmpeg($file,$onlyname){
	
$sec=getSec($file);
if($sec !=0){
$bit=getbit($sec);
$bit=$bit.'K';
}

//$command = 'ffmpeg -y -i `upload/'.$make.'` -c:v libx264 -preset medium -b:v $bit -pass 1 -b:a 128k -f mp4 /dev/null && \ffmpeg -i `upload/'.$make.'` -c:v libx264 -preset medium -b:v '.$bit.' -pass 2  -b:a 128k `upload/out/'.$onlyname.'.mp4` ';
$command = 'ffmpeg -y -i upload/'.$file.'  -vcodec libx264 -b:v '.$bit.' -vf scale=640:360 -pix_fmt yuv420p -c:a aac -strict experimental -b:a 128k -ac 2 -ar 44100 -threads 2 -f mp4 -crf 20 upload/out/'.$onlyname.'_con.mp4 -vstats 2>&1';
$command2 =('ffmpeg -i upload/'.$file.' -deinterlace -an -ss 2 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg upload/out/'.$onlyname.'_con.jpg 2>&1');

$info =exec($command);
$info2 =exec($command2);

return $info;
	
}
function uploadv($name,$tmp_name,$size){
$target_dir = "upload/";
$target_file = $target_dir . basename($name);
$onlyname=md5($name.time());
$uploadOk = 1;
$FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$make=$onlyname.".".$FileType;

if($size>79315000){
	$uploadOk = 0;
	$response_array=['status' => 'warning','response' => 'Please Keep your file Under 78MB'];
}
if($FileType != "mkv" && $FileType != "mp4" && $FileType != "mov"
&& $FileType != "avi" ) {
	$response_array=['status' => 'warning','response' => 'Video files are allowed here'];
    $uploadOk = 0;
}
 

if ($uploadOk == 0) {
	$response_array=$response_array;
// if everything is ok, try to upload file
}else {
    if (move_uploaded_file($tmp_name, "upload/".$make)) {
$response_array["status"]="success";
$info=ffmpeg($make,$onlyname);
$response_array["response"]='Video Uploaded Success';
$response_array["vid"]='upload/out/'.$onlyname.'_con.mp4';
$response_array["thumb"]='upload/out/'.$onlyname.'_con.jpg';
$finalsize=filesize($response_array["vid"]);
if($finalsize<24000000){
$urlfile='http://home.leadne.com/upload/out/'.$onlyname.'_con.mp4';

 $url = "https://graph.facebook.com/v2.11/me/message_attachments?access_token=EAAMc5FV9LwoBAKo7crzV3ZBe9mw7fWuLS4fJ8kZAg8sIyEcwFY9TVueNMpPtVEUqRor8dJAZA5rJSDbOqURL3FuuPaNTZCmxMcryn3UYM628hnZBQTEz3nPLnZAyr6DlkXXn8ZCsRQPDwFQIYFKdQu6ZCim3X4bJDQ0oKQ5dhJXgzAZDZD" ;
 $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => $url,
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS =>'{"message":{"attachment":{"type":"video", "payload":{"is_reusable": true,"url":"'.$urlfile.'"}}}}',
  CURLOPT_HTTPHEADER => array(
    "Cache-Control: no-cache",
    "Content-Type: application/json"),
));

$response =curl_exec($curl);

$err = curl_error($curl);

curl_close($curl);
$response = array(json_decode(($response),true));
if ($err) {
      $response_array['attachment_id']="cURL Error #:" . $err;

} else {
 $response_array['attachment_id']=$response[0]['attachment_id'];
$response_array['info']=$response;
}

	}else{
$response_array=['status' => 'Danger','response' =>'Please Keep your video under 5 mins or less '];

	}}else{
$response_array=['status' => 'Danger','response' =>'File Not Uploaded Reson #uploadv:Fun:292'];

    }
}
return $response_array;
}
function loginurl(){
print_r($_SESSION);
$fb = new Facebook\Facebook ([
  'app_id' => '876191942520586',
  'app_secret' => '5203c057410f39bebdfd13ac576bc173',
  'default_graph_version' => 'v2.11',
  ]);
 
$helper = $fb->getRedirectLoginHelper();
$accessToken = $helper->getAccessToken();
 



if (!isset($accessToken)) {
$permissions = ['email'];
$loginUrl = $helper->getLoginUrl('https://leadne.com/join', $permissions);
echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';	

	


}else{
	// Logged in
$_SESSION['fb_access_token'] = (string) $accessToken;
try {
  // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get("/10212817328779764/ids_for_pages?page=364529217264732",$accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}
 $helper5 = $fb->getPageTabHelper();

try {
  $accessToken = $helper5->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

// Logged in
echo '<h3>Page ID</h3>';
var_dump($helper5->getPageId());

echo '<h3>User is admin of page</h3>';
var_dump($helper5->isAdmin());

echo '<h3>Signed Request</h3>';
var_dump($helper5->getSignedRequest());

echo '<h3>Access Token</h3>';
var_dump($accessToken->getValue());
}
}
function facebookid(){

}






?>