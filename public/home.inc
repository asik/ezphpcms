<?php
$data=[];
$data=$this->data;
?>
<style>
.progression-studios-slider-display-table {
    background: -moz-linear-gradient(45deg, rgba(0, 123, 255, 0.86) 0%,#000000ab 47%,rgba(0, 0, 0, 0.62) 58%,#1083ffad 92%,rgba(0, 123, 255, 0.35) 100%);
    background: -webkit-linear-gradient(45deg, rgba(0, 123, 255, 0.86) 0%,#000000ab 47%,rgba(0, 0, 0, 0.62) 58%,#1083ffad 92%,rgba(0, 123, 255, 0.35) 100%);
    background: linear-gradient(45deg, rgba(0, 123, 255, 0.86) 0%,#000000ab 47%,rgba(0, 0, 0, 0.62) 58%,#1083ffad 92%,rgba(0, 123, 255, 0.35) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e6000000', endColorstr='#33000000',GradientType=1 );
}

a.progression-studios-slider-play-btn.afterglow {
 position: absolute;
    cursor: pointer;
    text-align: center;
    border-radius: 100px;
    right: 15vh;
    float: right;
    z-index: 12;
    width: 80px;
    height: 80px;
    line-height: 77px;
    top: 27vh;
    color: #ffffff;
    font-size: 40px;
    border: 1px solid #02020230;
    background: #ff003dba;
    box-shadow: inset 8px 10px 3px 0px rgba(0, 0, 0, 0.31);
}

	</style>
<div id="sidebar-bg">
  <?php
       include 'blade/_home_header';
	   include 'blade/_home_side_nav';
      ?>
<main id="col-main">
    <div class="dashboard-container">
        <div class="row">
<!--facebook VIDEO-->        	
            <div class="col-sm-12 col-md-6 col-lg-6">
                <li class="progression_studios_animate_in flex-active-slide" data-thumb-alt="" style="width: 100%;margin-right: -100%;position: relative;opacity: 1;display: block;z-index: 4;/* background: #fff; */">
         <div class="progression-studios-slider-dashboard-image-background" style="background-image:url(https://admin.leadne.com/<?php echo $data["news"]["news"][0]["img"];?>);">
                        <div class="progression-studios-slider-display-table">
						
                                                                                <a class="progression-studios-slider-play-btn afterglow" href="#VideoLightbox-db0"><i class="fas fa-play"></i></a>
                            <div class="progression-studios-slider-vertical-align">
							
                                <div class="container">
                                    <div class="progression-studios-slider-dashboard-caption-width">
                                        <div class="progression-studios-slider-caption-align">
                                            <h6 class="light-fonts-pro badge badge-success">New Arrivals</h6>
                                            <h2 class="light-fonts-pro"><?php echo $data["news"]["news"][0]["title"];?></h2>
                                        
                                            <p class="progression-studios-slider-description light-fonts-pro">
                                                <?php echo mb_substr(base64_decode($data["news"]["news"][0]["description"]),0,160,'UTF-8');?>
                                            </p>

                                            <div class="progression-studios-slider-more-options hide>
													<i class=" fas fa-ellipsis-h "></i>
													<ul>
														<li><a href="#! ">Add to Favorites</a></li>
														<li><a href="#! ">Add to Watchlist</a></li>
														<li><a href="#! ">Add to Playlist</a></li>
														<li><a href="#! ">Share...</a></li>
														<li><a href="#! ">Write A Review</a></li>
													</ul>
											</div>
											<div class="clearfix "></div>
												
												
												
											</div><!-- close .progression-studios-slider-caption-align -->
										</div><!-- close .progression-studios-slider-caption-width -->
									
									</div><!-- close .container -->
								</div><!-- close .progression-studios-slider-vertical-align -->
							</div><!-- close .progression-studios-slider-display-table -->
							<div class="progression-studios-slider-mobile-background-cover "></div>
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 d-none d-md-block">
			    <div class="row ">
			        <div class="col-6 col-sm-12 col-md-12 col-xl-6">
			        	 <li class="progression_studios_animate_in flex-active-slide" data-thumb-alt="" style="width: 100%;float: left;margin-right: -100%;position: relative;opacity: 1;display: block;z-index: 4;/* background: #fff; */">
     					    <div class="progression-studios-slider-dashboard-image-background" style="background-image:url(https://admin.leadne.com/<?php echo $data["news"]["news"][1]["img"];?>);background-position: center right;">
                        <div class="progression-studios-slider-display-table">
						
                             <a class="progression-studios-slider-play-btn afterglow" href="#VideoLightbox-db1"><i class="fas fa-play"></i></a>
                            <div class="progression-studios-slider-vertical-align">
                                <div class="container">
                                    
                                    <div class="progression-studios-slider-dashboard-caption-width">
                                        <div class="progression-studios-slider-caption-align">
                                            <h6 class="light-fonts-pro">New Arrivals</h6>
                                            <h4 class="light-fonts-pro"><?php echo $data["news"]["news"][1]["title"];?></h4>
                                            <ul class="progression-studios-slider-meta">
                                                <li>
                                                     <?php echo $data["news"]["news"][1]["date"];?>
                                                </li>
                                            </ul>
                                            <p class="progression-studios-slider-description light-fonts-pro">
                                                <?php echo mb_substr(base64_decode($data["news"]["news"][1]["description"]),0,160,'UTF-8');?>
                                            </p> 

											<div class="clearfix "></div>
											</div><!-- close .progression-studios-slider-caption-align -->
										</div><!-- close .progression-studios-slider-caption-width -->
									
									</div><!-- close .container -->
								</div><!-- close .progression-studios-slider-vertical-align -->
							</div><!-- close .progression-studios-slider-display-table -->
							<div class="progression-studios-slider-mobile-background-cover "></div>
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
 				</div>
			        <!-- close .col -->

			         <div class="col-6 col-md-12 col-xl-6 d-none d-xl-block">
			        	 <li class="progression_studios_animate_in flex-active-slide" data-thumb-alt="" style="width: 100%;float: left;margin-right: -100%;position: relative;opacity: 1;display: block;z-index: 4;/* background: #fff; */">
     					    <div class="progression-studios-slider-dashboard-image-background" style="background-image:url(https://admin.leadne.com/<?php echo $data["news"]["news"][2]["img"];?>);background-position: center right;">
                        <div class="progression-studios-slider-display-table">
						
                             <a class="progression-studios-slider-play-btn afterglow" href="#VideoLightbox-db2"><i class="fas fa-play"></i></a>
                            <div class="progression-studios-slider-vertical-align">
                                <div class="container">
                                    
                                    <div class="progression-studios-slider-dashboard-caption-width">
                                        <div class="progression-studios-slider-caption-align">
                                            <h6 class="light-fonts-pro">New Arrivals</h6>
                                            <h4 class="light-fonts-pro"><?php echo $data["news"]["news"][2]["title"];?></h4>
                                            <ul class="progression-studios-slider-meta">
                                                <li>
                                                     <?php echo $data["news"]["news"][2]["date"];?>
                                                </li>
                                            </ul>
                                            <p class="progression-studios-slider-description light-fonts-pro">
                                                <?php echo mb_substr(base64_decode($data["news"]["news"][2]["description"]),0,160,'UTF-8');?>
                                            </p>
											                                  

											<div class="clearfix "></div>
											</div><!-- close .progression-studios-slider-caption-align -->
										</div><!-- close .progression-studios-slider-caption-width -->
									
									</div><!-- close .container -->
								</div><!-- close .progression-studios-slider-vertical-align -->
							</div><!-- close .progression-studios-slider-display-table -->
							<div class="progression-studios-slider-mobile-background-cover "></div>
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
 				</div>
			   
			  
			    </div>
			</div>
		</div>

<!--UPLOADED VIDEO-->
		<h4 class="heading-extra-margin-bottom">Leadne Featured</h4>
		<div class="row">
			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb0" class="afterglow"><img src="<?php echo $data["fb"][0]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][0]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb1" class="afterglow"><img src="<?php echo $data["fb"][1]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][1]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb2" class="afterglow"><img src="<?php echo $data["fb"][2]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][2]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb3" class="afterglow"><img src="<?php echo $data["fb"][3]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][3]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb4" class="afterglow"><img src="<?php echo $data["fb"][4]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][4]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb5" class="afterglow"><img src="<?php echo $data["fb"][5]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][5]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb6" class="afterglow"><img src="<?php echo $data["fb"][6]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][6]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      			   		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3">
			            <div class="item-listing-container-skrn">
			                <div class="item-listing-text-skrn">
							<a href="#VideoLightbox-fb7" class="afterglow"><img src="<?php echo $data["fb"][7]["image_url"];?>" alt="Listing"></a>
			                <p><a href="dashboard-movie-profile.html"><?php echo $data["fb"][7]["title"];?></a></p>
			                </div>
			                <!-- close .item-listing-text-skrn -->
			            </div>
			            <!-- close .item-listing-container-skrn -->
			        </div>
			
			        
			        <!-- close .col -->
			      	
			
			    
			    </div>
			</div>
			<h4 class="heading-extra-margin-bottom">Action</h4>
			<!-- close .row -->
			<ul class="page-numbers">
			    <li><a class="previous page-numbers" href="#!"><i class="fas fa-chevron-left"></i></a></li>
			    <li><span class="page-numbers current">1</span></li>
			    <li><a class="page-numbers" href="#!">2</a></li>
			    <li><a class="page-numbers" href="#!">3</a></li>
			    <li><a class="page-numbers" href="#!">4</a></li>
			    <li><a class="next page-numbers" href="#!"><i class="fas fa-chevron-right"></i></a></li>
			</ul>
			</div>
			</div>
			<div class="clearfix"></div>
			<!-- close .progression-studios-slider - See /js/script.js file for options -->
			<ul class="dashboard-genres-pro">
			    <li>
			        <img src="asset/images/genres/drama.png" alt="Drama">
			        <h6>Drama</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/comedy.png" alt="Comedy">
			        <h6>Comedy</h6>
			    </li>
			    <li class="active">
			        <img src="asset/images/genres/action.png" alt="Action">
			        <h6>Action</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/romance.png" alt="Romance">
			        <h6>Romance</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/horror.png" alt="Horror">
			        <h6>Horror</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/fantasy.png" alt="Fantasy">
			        <h6>Fantasy</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/sci-fi.png" alt="Sci-Fi">
			        <h6>Sci-Fi</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/thriller.png" alt="Thriller">
			        <h6>Thriller</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/western.png" alt="Western">
			        <h6>Western</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/adventure.png" alt="Adventure">
			        <h6>Adventure</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/animation.png" alt="Animation">
			        <h6>Animation</h6>
			    </li>
			    <li>
			        <img src="asset/images/genres/documentary.png" alt="Documentary">
			        <h6>Documentary</h6>
			    </li>
			</ul>
			<div class="clearfix"></div>
<!-- close .dashboard-container -->
</main>
     <video id="VideoLightbox-db0" poster="https://admin.leadne.com/<?php echo $data["news"]["news"][0]["img"];?>" width="960" height="540" class="afterglow-lightboxplayer" data-autoresize="fit">
									<source src="https://admin.leadne.com/<?php echo replacecon($data["news"]["news"][0]["video"]);?>" type="video/mp4">
									<source src="https://admin.leadne.com/<?php echo $data["news"]["news"][0]["video"];?>" type="video/mp4">

                                    </video>
									  <video id="VideoLightbox-db1" poster="https://admin.leadne.com/<?php echo $data["news"]["news"][1]["img"];?>" width="960" height="540" class="afterglow-lightboxplayer" data-autoresize="fit">
									<source src="https://admin.leadne.com/<?php echo replacecon($data["news"]["news"][1]["video"]);?>" type="video/mp4">

                                    <source src="https://admin.leadne.com/<?php echo $data["news"]["news"][1]["video"];?>" type="video/mp4">
                                    </video>
									  <video id="VideoLightbox-db2" poster="https://admin.leadne.com/<?php echo $data["news"]["news"][2]["img"];?>" width="960" height="540" class="afterglow-lightboxplayer" data-autoresize="fit">
										<source src="https://admin.leadne.com/<?php echo replacecon($data["news"]["news"][2]["video"]);?>" type="video/mp4">
                                        <source src="https://admin.leadne.com/<?php echo $data["news"]["news"][2]["video"];?>" type="video/mp4">
                                    </video>
									<video id="VideoLightbox-fb0" poster="<?php echo $data["fb"][0]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][0]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][0]["source"];?>" type="video/mp4">
                                    </video>
   <video id="VideoLightbox-fb1" poster="<?php echo $data["fb"][1]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][1]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][1]["source"];?>" type="video/mp4">
                                    </video>
                                       <video id="VideoLightbox-fb2" poster="<?php echo $data["fb"][2]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][2]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][2]["source"];?>" type="video/mp4">
                                    </video>
                                       <video id="VideoLightbox-fb3" poster="<?php echo $data["fb"][3]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][3]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][3]["source"];?>" type="video/mp4">
                                    </video>
                                       <video id="VideoLightbox-fb4" poster="<?php echo $data["fb"][4]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][4]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][4]["source"];?>" type="video/mp4">
                                    </video>
                                       <video id="VideoLightbox-fb5" poster="<?php echo $data["fb"][5]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][5]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][5]["source"];?>" type="video/mp4">
                                    </video>
                                       <video id="VideoLightbox-fb6" poster="<?php echo $data["fb"][6]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][6]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][6]["source"];?>" type="video/mp4">
                                    </video>
                                       <video id="VideoLightbox-fb7" poster="<?php echo $data["fb"][7]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][7]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][7]["source"];?>" type="video/mp4">
                                    </video>
                                       <video id="VideoLightbox-fb8" poster="<?php echo $data["fb"][8]["image_url"];?>" width="640" height="640" class="afterglow-lightboxplayer" data-autoresize="fit">
                                        <source src="<?php echo $data["fb"][8]["source"];?>" type="video/mp4">
                                        <source src="<?php echo $data["fb"][8]["source"];?>" type="video/mp4">
                                    </video>
<!-- Modal -->
     
