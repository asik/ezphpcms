<?php
// Pass session data over. Only needed if not already passed by another script like WordPress.
if(!session_id()) {
    session_start();
} 
// Include the required dependencies.
require_once 'vendor/Facebook/autoload.php';
 
// Initialize the Facebook PHP SDK v5.
$fb = new Facebook\Facebook ([
  'app_id' => '876191942520586',
  'app_secret' => '03d258dd8f1688d3128dbce7d366ba6e',
  'default_graph_version' => 'v2.11',
  ]);
 
$helper = $fb->getRedirectLoginHelper();
 
$permissions = ['email']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://leadne.com/fbcallback.php', $permissions);
 
echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';