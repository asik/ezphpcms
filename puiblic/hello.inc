    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="../dashboard.html">Leadne News Portal</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="../dashboard.html">
                            <i class="material-icons">dashboard</i>
                            Dashboard
                        </a>
                    </li>
                    <li class= "">
                        <a href="register.html">
                            <i class="material-icons">person_add</i>
                            Register
                        </a>
                    </li>
                    <li class= " active ">
                        <a href="login.html">
                            <i class="material-icons">fingerprint</i>
                            Login
                        </a>
                    </li>
                    <li class= "">
                        <a href="lock.html">
                            <i class="material-icons">lock_open</i>
                            Lock
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="wrapper wrapper-full-page">
   <div class="full-page login-page" filter-color="black" data-image="/assets/img/abstract-art-wallpaper-high-quality.jpg">

        <!--   you can change the color of the filter page using: data-color="blue | green | orange | red | purple" -->

		
		<div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form id="LoginValidation" method="post" action="/post?do=login" >
							
							        <div class="card card-profile hidden">
								
                        <div class="card-avatar">
                            <a href="#pablo">
                                <img class="avatar" src="/assets/img/logo/loginlogo.png" alt="...">
                            </a>
                        </div>	
                       <div class="card-content">
					                                           <h3 class="card-title text-success">Login</h3>

                                
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>

                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">Email address</label>
                                                <input  class="form-control" name="email"
			                                   type="text"
			                                   email="true"
			                                    >
                                            <span class="material-input"></span></div>
                                        </div>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">Password</label>
                                                <input class="form-control"  name="password"
			                                   type="password"
			                                    >
                                            <span class="material-input"></span></div>
                                        </div>
														                        <div class="category form-category"><star>*</star> Required fields</div>

                                    </div>
                                    
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-round">Login</button>
                        </div>
                    </div>
		 					
							
                             
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		
 <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                   Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        © <script async="" src="//www.google-analytics.com/analytics.js"></script><script>document.write(new Date().getFullYear())</script>2017 <a href="http://www.leadne.com/">Leadne</a>, made with ♥ for a better web
                    </p>
                </div>
            </footer>
		
